package com.fistforge.cruisecontrol;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.fistforge.cruisecontrol.R;

public class MeterLayout extends FrameLayout {

	private static final double backgroundWidth = 1280;
	private static final double backgroundHeight = 671;
	private static final double pinWidth = 0;
	public static double pinLiftPixelsX = 0;
	public static double pinLiftPixelsY = 0;
	
	public MeterLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		
		double width = r - l;
		double height = b - t;
		boolean marginIsAboveAndBelow = (width / height) - (backgroundWidth / backgroundHeight) <= 0;
		int marginVertical = 0;
		int marginHorizontal = 0;
		if(marginIsAboveAndBelow){
			double factor = (backgroundWidth / width);
			marginVertical = (int) ((height - (backgroundHeight / factor)) / 2);
		} else {
			double factor = (backgroundHeight / height);
			marginHorizontal = (int) ((width - (backgroundWidth / factor)) / 2);
		}
		
		int pinLeft 	= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (143 / backgroundWidth));
		int pinRight 	= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (659 / backgroundWidth));
		int pinTop 		= (int)   (marginVertical   + (height - marginVertical*2)   * (585 / backgroundHeight));
		int pinBottom 	= (int)   (marginVertical   + (height - marginVertical*2)   * (606 / backgroundHeight));
		float pinPivotX = (float) ((width - marginHorizontal*2) * ((641-143) / backgroundWidth));
		float pinPivotY = (float) ((pinBottom - pinTop) / 2);
		
		int cruiseLeft 		= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (84  / backgroundWidth));
		int cruiseRight 	= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (165 / backgroundWidth));
		int cruiseTop	 	= (int)   (marginVertical   + (height - marginVertical*2)   * (540 / backgroundHeight));
		int cruiseBottom 	= (int)   (marginVertical   + (height - marginVertical*2)   * (592 / backgroundHeight));
		float cruisePivotX  = (float) ((width  - marginHorizontal*2) * ((641-84) / backgroundWidth));
		float cruisePivotY	= (float) ((height - marginVertical*2)   * (55  / backgroundHeight));
		
		int cruiseLowerLeft 		= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (84  / backgroundWidth));
		int cruiseLowerRight 	= (int)   (marginHorizontal + (width  - marginHorizontal*2) * (165 / backgroundWidth));
		int cruiseLowerTop	 	= (int)   (marginVertical   + (height - marginVertical*2)   * (594 / backgroundHeight));
		int cruiseLowerBottom 	= (int)   (marginVertical   + (height - marginVertical*2)   * (646 / backgroundHeight));
		float cruiseLowerPivotX  = (float) ((width  - marginHorizontal*2) * ((641-84) / backgroundWidth));
		float cruiseLowerPivotY	= (float) ((height - marginVertical*2)   * (1  / backgroundHeight));
		
		int speed40Left		= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (297  / backgroundWidth));
		int speed40Right	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (407  / backgroundWidth));
		int speed40Top	 	= (int) (marginVertical   + (height - marginVertical  *2) * (250 / backgroundHeight));
		int speed40Bottom 	= (int) (marginVertical   + (height - marginVertical  *2) * (350 / backgroundHeight));
		
		int speed60Left		= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (435  / backgroundWidth));
		int speed60Right	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (545  / backgroundWidth));
		int speed60Top	 	= (int)   (marginVertical   + (height - marginVertical*2) * (160 / backgroundHeight));
		int speed60Bottom 	= (int)   (marginVertical   + (height - marginVertical*2) * (260 / backgroundHeight));
		
		int speed80Left		= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (586  / backgroundWidth));
		int speed80Right	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (701  / backgroundWidth));
		int speed80Top	 	= (int)   (marginVertical   + (height - marginVertical*2) * (127 / backgroundHeight));
		int speed80Bottom 	= (int)   (marginVertical   + (height - marginVertical*2) * (227 / backgroundHeight));
		
		int speed100Left	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (735  / backgroundWidth));
		int speed100Right	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (850  / backgroundWidth));
		int speed100Top	 	= (int)   (marginVertical   + (height - marginVertical*2) * (160 / backgroundHeight));
		int speed100Bottom 	= (int)   (marginVertical   + (height - marginVertical*2) * (260 / backgroundHeight));
		
		int speed120Left	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (863  / backgroundWidth));
		int speed120Right	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (973  / backgroundWidth));
		int speed120Top	 	= (int)   (marginVertical   + (height - marginVertical*2) * (250 / backgroundHeight));
		int speed120Bottom 	= (int)   (marginVertical   + (height - marginVertical*2) * (350 / backgroundHeight));

        int logoLeft	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (935  / backgroundWidth));
        int logoRight	= (int)	(marginHorizontal + (width  - marginHorizontal*2) * (1290  / backgroundWidth));
        int logoTop	 	= (int)   (marginVertical   + (height - marginVertical*2) * (15 / backgroundHeight));
        int logoBottom 	= (int)   (marginVertical   + (height - marginVertical*2) * (148 / backgroundHeight));
		
		pinLiftPixelsX = (width  - marginHorizontal*2) * (25 / backgroundWidth);
		pinLiftPixelsY = (height - marginVertical  *2) * (25 / backgroundHeight);
		
		for(int i = 0 ; i < this.getChildCount() ; i++){
			View view = this.getChildAt(i);
			int id = view.getId();
			switch (id){
				case R.id.background :
					view.layout(l, t, r, b);
					break;
				case R.id.pin: 
					view.layout(pinLeft, pinTop, pinRight, pinBottom);
					view.setPivotX(pinPivotX);
					view.setPivotY(pinPivotY);
					break;
				case R.id.cruise_speed_indicator_imageview: 
					view.layout(cruiseLeft, cruiseTop, cruiseRight, cruiseBottom);
					view.setPivotX(cruisePivotX);
					view.setPivotY(cruisePivotY);
					break;
				case R.id.cruise_speed_lower_indicator_imageview: 
					view.layout(cruiseLowerLeft, cruiseLowerTop, cruiseLowerRight, cruiseLowerBottom);
					view.setPivotX(cruiseLowerPivotX);
					view.setPivotY(cruiseLowerPivotY);
					break;
				case R.id.light1_imageview:
					view.layout(l, t, r, b);
					break;
				case R.id.light2_imageview:
					view.layout(l, t, r, b);
					break;
				case R.id.preset_speed_40_button:
					view.layout(speed40Left, speed40Top, speed40Right, speed40Bottom);
					break;
				case R.id.preset_speed_60_button:
					view.layout(speed60Left, speed60Top, speed60Right, speed60Bottom);
					break;
				case R.id.preset_speed_80_button:
					view.layout(speed80Left, speed80Top, speed80Right, speed80Bottom);
					break;
				case R.id.preset_speed_100_button:
					view.layout(speed100Left, speed100Top, speed100Right, speed100Bottom);
					break;
				case R.id.preset_speed_120_button:
					view.layout(speed120Left, speed120Top, speed120Right, speed120Bottom);
					break;
                case R.id.logo_button:
                    view.layout(logoLeft, logoTop, logoRight, logoBottom);
                    break;

				case R.id.button1:
					view.layout(10, 80, 140, 210);
					break;
				case R.id.button2:
					view.layout(10, 220, 140, 350);
					break;
				case R.id.debugText1:
					view.layout(150, 40, 400, 80);
					break;
				case R.id.debugText2:
					view.layout(150, 90, 400, 130);
					break;
				case R.id.debugText3:
					view.layout(150, 140, 400, 180);
					break;
				default : Log.d("cc", "Some view at position " + i); 
					break;
			}
		}	
	}
}
