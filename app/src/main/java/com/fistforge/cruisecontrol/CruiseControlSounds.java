package com.fistforge.cruisecontrol;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

public class CruiseControlSounds {

	private static final int SOUND_DELAY = 5000;
	
	private MediaPlayer[] mUnderSpeeds;
	private MediaPlayer[] mOverSpeeds;

	private Handler mHandler;
	private Runnable mPendingUnderSpeed = null;
	private Runnable mPendingOverSpeed = null;
	private String TAG = "cc-sound";

	public CruiseControlSounds(Context ctx) {
		mUnderSpeeds = new MediaPlayer[] {
                 MediaPlayer.create(ctx,R.raw.car_horn_1),
				 MediaPlayer.create(ctx,R.raw.car_horn_2),
				 MediaPlayer.create(ctx,R.raw.car_horn_3),
				 MediaPlayer.create(ctx,R.raw.car_horn_4),
				 MediaPlayer.create(ctx,R.raw.car_horn_5)};
		mOverSpeeds = new MediaPlayer[] {
				MediaPlayer.create(ctx, R.raw.police_siren_1),
				MediaPlayer.create(ctx, R.raw.police_siren_2),
				MediaPlayer.create(ctx, R.raw.police_siren_3),
				MediaPlayer.create(ctx, R.raw.police_siren_4),
				MediaPlayer.create(ctx, R.raw.police_siren_5)};

		mHandler = new Handler();
	}

	public void playOverSpeed() {
		if (mPendingUnderSpeed != null) {
			mHandler.removeCallbacks(mPendingUnderSpeed);
			mPendingUnderSpeed = null;
		}

		MediaPlayer underSpeedPlaying = currentlyPlayingUnderSpeed();
		if (underSpeedPlaying != null) {
			underSpeedPlaying.stop();
		}

		if (currentlyPlayingOverSpeed() == null && mPendingOverSpeed == null) {

			final MediaPlayer sound = mOverSpeeds[(int) Math.floor(Math
					.random() * mOverSpeeds.length)];

			mPendingOverSpeed = new Runnable() {

				@Override
				public void run() {
					sound.start();
					mPendingOverSpeed = null;
				}
			};

			mHandler.postDelayed(mPendingOverSpeed, SOUND_DELAY);

			sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
                    try {
                        mp.stop();
                        mp.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CruiseControlSounds.this.playOverSpeed();
				}
			});
		}
	}

	public void playUnderSpeed() {
		if (mPendingOverSpeed != null) {
			mHandler.removeCallbacks(mPendingOverSpeed);
			mPendingOverSpeed = null;
		}

		MediaPlayer overSpeedPlaying = currentlyPlayingOverSpeed();
		if (overSpeedPlaying != null) {
			overSpeedPlaying.stop();
		}

		if (currentlyPlayingUnderSpeed() == null && mPendingUnderSpeed == null) {

			final MediaPlayer sound = mUnderSpeeds[(int) Math.floor(Math
					.random() * mUnderSpeeds.length)];

			mPendingUnderSpeed = new Runnable() {

				@Override
				public void run() {
					sound.start();
					mPendingUnderSpeed = null;
				}
			};

			mHandler.postDelayed(mPendingUnderSpeed, SOUND_DELAY);

			sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
                    try {
                        mp.stop();
                        mp.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CruiseControlSounds.this.playUnderSpeed();
				}
			});
		}
	}

	public void stop() {
		if (mPendingOverSpeed != null) {
			mHandler.removeCallbacks(mPendingOverSpeed);
			mPendingOverSpeed = null;
		}
		if (mPendingUnderSpeed != null) {
			mHandler.removeCallbacks(mPendingUnderSpeed);
			mPendingUnderSpeed = null;
		}

		MediaPlayer sound = currentlyPlayingOverSpeed();
		if (sound != null) {
            try {
                sound.stop();
                sound.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

		sound = currentlyPlayingUnderSpeed();
		if (sound != null) {

            try {
                sound.stop();
                sound.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

	private MediaPlayer currentlyPlayingUnderSpeed() {

		for (int i = 0; i < mUnderSpeeds.length; i++) {
			MediaPlayer media = mUnderSpeeds[i];
			if (media.isPlaying()) {
				return media;
			}
		}

		return null;
	}

	private MediaPlayer currentlyPlayingOverSpeed() {

		for (int i = 0; i < mOverSpeeds.length; i++) {
			MediaPlayer media = mOverSpeeds[i];
			if (media.isPlaying()) {
				return media;
			}
		}

		return null;
	}
}
