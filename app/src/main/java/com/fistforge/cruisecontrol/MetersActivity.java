package com.fistforge.cruisecontrol;

import com.fistforge.cruisecontrol.R;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.animation.AnimatorListenerAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MetersActivity extends Activity {

	public static final boolean DEBUG = false;

	private ImageView mPin;
	private ImageView mCruiseSpeedIndicator;
	private ImageView mCruiseSpeedLowerIndicator;
	private ImageView mLight1;
	private ImageView mLight2;

	private ObjectAnimator mPinAnimation;
	private AnimatorSet mAlarmAnimatorOverSpeed;
	private ObjectAnimator mLight1AlphaAnimator1;
	private ObjectAnimator mLight1AlphaAnimator2;
	private ObjectAnimator mAlarmAnimatorUnderSpeed;

	private boolean mActionBarShowing = false;
	private boolean mOnClick = false;
	private double mTouchActionDownX = 0;
	private double mTouchActionDownY = 0;
	private double mLastPointerAngle = 0;

	public static final float maxSpeed = 160; // gauge's max value

	private int w;
	private int h;

	private float mCurrentGpsSpeed = 0;
	private double mCurrentGpsSpeedAngle = 0;
	private double mCruiseSpeedIndicatorAngle = 0;

	private TextView dText1;
	private TextView dText2;
	private TextView dText3;

	private UnitDialogFragment mUnitDialog;

	private TextView dButton1;

	private TextView dButton2;




	public static boolean mSoundsOn 				= true;
	public static boolean mVibrationOn 				= false;
	public static boolean mLightsOn 				= false;
	public static boolean mImperial 				= false;

	private static final int MENUITEM_SOUND 		= 0;
	private static final int MENUITEM_VIBRATION 	= 1;
	private static final int MENUITEM_UNIT 			= 2;
	private static final int MENUITEM_HELP			= 3;
	private static final int MENUITEM_DEBUG 		= 4;

	private static final double CLICK_THRESHOLD 	= 3;

	private static final int REQUESTCODE_TUTORIAL 	= 100;
	private static final int REQUESTCODE_HELP		= 101;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences settings = getSharedPreferences(CruiseControlService.SETTINGS_FILE, MODE_PRIVATE);
		if(settings.getBoolean(CruiseControlService.SETTINGS_FIRST_LAUNCH, true)){
			this.startActivityForResult(new Intent(this, TutorialActivity.class), REQUESTCODE_TUTORIAL);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		} else {
			initialize();
		}
	}
	
	private void initialize(){
		setContentView(R.layout.activity_meters);
		Log.d("cc", this.getClass().getName() + ".onCreate");

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		w = size.x;
		h = size.y;

		IntentFilter filter = new IntentFilter();
		filter.addAction(CruiseControlService.ACTION_SPEED);
		filter.addAction(CruiseControlService.ACTION_INRANGE);
		filter.addAction(CruiseControlService.ACTION_OVERSPEED);
		filter.addAction(CruiseControlService.ACTION_UNDERSPEED);
		filter.addAction(CruiseControlService.ACTION_SHUTDOWN);
		filter.addAction(CruiseControlService.ACTION_INACTIVE);
		registerReceiver(mBroadcastReceiver, filter);

		Intent ccServiceIntent = new Intent(this, CruiseControlService.class);
		startService(ccServiceIntent);
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		SharedPreferences settings = getSharedPreferences(CruiseControlService.SETTINGS_FILE, MODE_PRIVATE);
		mSoundsOn 		= settings.getBoolean(CruiseControlService.SETTINGS_SOUNDS_ON, mSoundsOn);
		mVibrationOn 	= settings.getBoolean(CruiseControlService.SETTINGS_VIBRATION_ON, mVibrationOn);
//		mLightsOn 		= settings.getBoolean(CruiseControlService.SETTINGS_LIGHTS_ON, true);
		mImperial 		= settings.getBoolean(CruiseControlService.SETTINGS_UNIT_IMPERIAL, mImperial);

		try {
			setTitle(getResources().getText(R.string.app_name) + " v"
					+ getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		getActionBar().hide();
		
		mUnitDialog = new UnitDialogFragment();

		mPin = (ImageView) findViewById(R.id.pin);
		mCruiseSpeedIndicator = (ImageView) findViewById(R.id.cruise_speed_indicator_imageview);
		mCruiseSpeedLowerIndicator = (ImageView) findViewById(R.id.cruise_speed_lower_indicator_imageview);
		
		dButton1 = (TextView) findViewById(R.id.button1);
		dButton2 = (TextView) findViewById(R.id.button2);
		dText1 = (TextView) findViewById(R.id.debugText1);
		dText2 = (TextView) findViewById(R.id.debugText2);
		dText3 = (TextView) findViewById(R.id.debugText3);

		mPinAnimation = ObjectAnimator.ofFloat(mPin, "rotation", 0, 0);
        mPinAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		mPinAnimation.setDuration(1000);

		mLight1 = (ImageView) findViewById(R.id.light1_imageview);
		mLight1AlphaAnimator1 = ObjectAnimator.ofFloat(mLight1, "alpha", 0,0.8f, 0);
		mLight1AlphaAnimator1.setDuration(200);
		mLight1AlphaAnimator1.setInterpolator(new LinearInterpolator());
		mLight1AlphaAnimator2 = ObjectAnimator.ofFloat(mLight1, "alpha", 0, 1,0);
		mLight1AlphaAnimator2.setDuration(600);
		mLight1AlphaAnimator2.setInterpolator(new DecelerateInterpolator());
		mAlarmAnimatorOverSpeed = new AnimatorSet();
		mAlarmAnimatorOverSpeed.playSequentially(mLight1AlphaAnimator1,mLight1AlphaAnimator2);
		mAlarmAnimatorOverSpeed.addListener(mAnimatorListener);
		
		mLight2 = (ImageView) findViewById(R.id.light2_imageview);
		mAlarmAnimatorUnderSpeed = ObjectAnimator.ofFloat(mLight2, "alpha", 0, 0.7f, 0);
		mAlarmAnimatorUnderSpeed.setDuration(1500);
		mAlarmAnimatorUnderSpeed.setInterpolator(new LinearInterpolator());
		mAlarmAnimatorUnderSpeed.setRepeatCount(ValueAnimator.INFINITE);
//		mAlarmAnimatorOverSpeed.addListener(mAnimatorListener);
	}
	
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUESTCODE_TUTORIAL){
			SharedPreferences.Editor settings = getSharedPreferences(CruiseControlService.SETTINGS_FILE, Context.MODE_PRIVATE).edit();
			settings.putBoolean(CruiseControlService.SETTINGS_FIRST_LAUNCH, false);
			settings.commit();
			initialize();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		float x = event.getX();
		float y = event.getY();
		int action = event.getAction();
		
		double currentPointerAngle = x <= w / 2 ? 
				 					 Math.atan((h - y) / ((w / 2) - x)) : 
				 					 Math.PI - Math.atan((h - y) / (x - (w / 2)));

		if (action == MotionEvent.ACTION_DOWN) {
			// CLICK OR DRAG START //
			mOnClick = true;
			mTouchActionDownX = x;
			mTouchActionDownY = y;
			mLastPointerAngle = currentPointerAngle;
			cruiseSpeedSetToAngle(mCruiseSpeedIndicatorAngle, true);
		} else if (action == MotionEvent.ACTION_UP) {
			if (mOnClick) {
				// CLICK //

				mOnClick = false;
			} else {
				// DRAG STOP //
				
			}
			double newAngle = mCruiseSpeedIndicatorAngle + currentPointerAngle - mLastPointerAngle;
			cruiseSpeedSetToAngle(newAngle, false);
		} else if (action == MotionEvent.ACTION_MOVE) {
			if (Math.abs(mTouchActionDownX - x) > CLICK_THRESHOLD || Math.abs(mTouchActionDownY - y) > CLICK_THRESHOLD) {
				// DRAG //
				mOnClick = false;
				double newAngle = mCruiseSpeedIndicatorAngle + currentPointerAngle - mLastPointerAngle;
				mLastPointerAngle = currentPointerAngle;
				cruiseSpeedSetToAngle(newAngle, true);
			}
		}
		return super.onTouchEvent(event);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d("cc", this.getClass().getName() + ".onStart");
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(!getSharedPreferences(CruiseControlService.SETTINGS_FILE, MODE_PRIVATE).getBoolean(CruiseControlService.SETTINGS_FIRST_LAUNCH, true)){
			bindService(new Intent(this, CruiseControlService.class), mConnection,Service.BIND_AUTO_CREATE);
		}
		Log.d("cc", this.getClass().getName() + ".onResume");
	}

	@Override
	protected void onPause() {
		Log.d("cc", this.getClass().getName() + ".onPause");
		if(mCruiseControlService != null){
			mCruiseControlService.setIsBoundToActivity(false);
			mCruiseControlService.updateState();
			unbindService(mConnection);
		}
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.d("cc", this.getClass().getName() + ".onStop");
		super.onStop();
	}

    @Override
    public void onBackPressed() {
        stopService(new Intent(this, CruiseControlService.class));
        super.onBackPressed();
    }

    @Override
	protected void onDestroy() {
		Log.d("cc", this.getClass().getName() + ".onDestroy");		
		if (mBroadcastReceiver != null) unregisterReceiver(mBroadcastReceiver);
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inf = getMenuInflater();
		inf.inflate(R.menu.meter_activity_actions, menu);
		menu.getItem(MENUITEM_SOUND).setIcon(mSoundsOn ? R.drawable.ic_action_volume_on: R.drawable.ic_action_volume_muted);
		menu.getItem(MENUITEM_VIBRATION).setIcon(mVibrationOn ? R.drawable.vibrate_on : R.drawable.vibrate_off);
		menu.getItem(MENUITEM_UNIT).setTitle(getResources().getString(R.string.units));
		menu.getItem(MENUITEM_HELP).setTitle(getResources().getString(R.string.help));
		if(DEBUG){   
			menu.add(R.string.debug).setOnMenuItemClickListener(new OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					int visibility = MetersActivity.this.dButton1.getVisibility()==View.GONE? View.VISIBLE : View.GONE;
					
					MetersActivity.this.dButton1.setVisibility(visibility);
					MetersActivity.this.dButton2.setVisibility(visibility);
					MetersActivity.this.dText1.setVisibility(visibility);
					MetersActivity.this.dText2.setVisibility(visibility);
					MetersActivity.this.dText3.setVisibility(visibility);
					
					return false;
				}
			});
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		SharedPreferences.Editor settings = getSharedPreferences(CruiseControlService.SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		switch (item.getItemId()) {
		case R.id.action_sound:
			mSoundsOn = !mSoundsOn;
			item.setIcon(mSoundsOn ? R.drawable.ic_action_volume_on: R.drawable.ic_action_volume_muted);
			settings.putBoolean(CruiseControlService.SETTINGS_SOUNDS_ON,mSoundsOn);
			settings.commit();
			mCruiseControlService.updateState();
			return true;
		case R.id.action_vibration:
			mVibrationOn = !mVibrationOn;
			item.setIcon(mVibrationOn ? R.drawable.vibrate_on: R.drawable.vibrate_off);
			settings.putBoolean(CruiseControlService.SETTINGS_VIBRATION_ON,mVibrationOn);
			settings.commit();
			mCruiseControlService.updateState();
			return true;
		case R.id.action_unit:
			mUnitDialog.show(getFragmentManager(), "unit");
			return true;
		case R.id.action_help:
			startActivityForResult(new Intent(this, TutorialActivity.class), REQUESTCODE_HELP);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	private void cruiseSpeedSetToAngle(double radians, boolean adjusting){
		mCruiseSpeedIndicatorAngle = radians<0 ? 0 :
									 radians>Math.PI ? Math.PI :
									 radians;
		double upperAngle = mCruiseSpeedIndicatorAngle + 0.5f * Math.PI*(metersPerSecondToAngle(mCruiseControlService.cruiserTolerance())/180);
		double lowerAngle = mCruiseSpeedIndicatorAngle - 0.5f * Math.PI*(metersPerSecondToAngle(mCruiseControlService.cruiserTolerance())/180);
		if(!adjusting){
			mCruiseSpeedIndicator.setTranslationX(0);
			mCruiseSpeedIndicator.setTranslationY(0);
			mCruiseSpeedLowerIndicator.setTranslationX(0);
			mCruiseSpeedLowerIndicator.setTranslationY(0);
			
			mCruiseControlService.setCruiseSpeed(currentUnitToMetersPerSecond((float)(mCruiseSpeedIndicatorAngle/Math.PI)*maxSpeed));
			mCruiseControlService.updateState();
			
			dText2.setText(String.format("%.1f", (float)(mCruiseSpeedIndicatorAngle/Math.PI)*maxSpeed));
		} else {
			mCruiseSpeedIndicator.setTranslationX(-(float) (Math.cos(upperAngle) * MeterLayout.pinLiftPixelsX));
			mCruiseSpeedIndicator.setTranslationY(-(float) (Math.sin(upperAngle) * MeterLayout.pinLiftPixelsY));
			mCruiseSpeedLowerIndicator.setTranslationX(-(float) (Math.cos(lowerAngle) * MeterLayout.pinLiftPixelsX));
			mCruiseSpeedLowerIndicator.setTranslationY(-(float) (Math.sin(lowerAngle) * MeterLayout.pinLiftPixelsY));
		}
		
		mCruiseSpeedIndicator.setRotation((float)(upperAngle*180.0/Math.PI));
		mCruiseSpeedLowerIndicator.setRotation((float)(lowerAngle*180.0/Math.PI)); 
	}

	public void speed40(View v) {
		cruiseSpeedSetToAngle(Math.PI * (45.0 / 180.0), false);
	}
	
	public void speed60(View v) {
		cruiseSpeedSetToAngle(Math.PI * (67.5 / 180.0), false);
	}
	
	public void speed80(View v) {
		cruiseSpeedSetToAngle(Math.PI * (90.0 / 180.0), false);
	}
	
	public void speed100(View v) {
		cruiseSpeedSetToAngle(Math.PI * (112.5 / 180.0), false);
	}
	
	public void speed120(View v) {
		cruiseSpeedSetToAngle(Math.PI * (135.0 / 180.0), false);
	}

    public void logoTouched(View v) {
        if (!mActionBarShowing) {
            getActionBar().show();
            mActionBarShowing = true;
            this.getWindow().getDecorView().getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getActionBar().hide();
                    mActionBarShowing = false;
                }
            }, 3000);
        }
    }
	
	private float currentUnitToMetersPerSecond(float speedInCurrentUnit){
		return speedInCurrentUnit / (!mImperial ? 3.6f : 2.23693629f);
	}
	
	public void speedUp(View v) {
		Intent intent = new Intent("poiu");
		float speed = mCurrentGpsSpeed + currentUnitToMetersPerSecond(5);
		intent.putExtra(CruiseControlService.EXTRA_SPEED, speed);
		mCruiseControlService.updateState();
		sendBroadcast(intent);
	}

	public void speedDown(View v) {
		Intent intent = new Intent("poiu");
		float speed = mCurrentGpsSpeed - currentUnitToMetersPerSecond(5);;
		intent.putExtra(CruiseControlService.EXTRA_SPEED, speed);
		mCruiseControlService.updateState();
		sendBroadcast(intent);
	}

	private double metersPerSecondToAngle(double speed) {
		double speedInUnit = speed * (!mImperial ? 3.6f : 2.23693629f);
		return (speedInUnit / maxSpeed) * 180;
	}

	private float angleToSpeed(double angle) {
		return currentUnitToMetersPerSecond( (float) (maxSpeed * angle / 180) );
	}

	AnimatorListener mAnimatorListener = new AnimatorListener();

	private class AnimatorListener extends AnimatorListenerAdapter {

		private boolean mCanceled;

		@Override
		public void onAnimationStart(Animator animation) {
			mCanceled = false;
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			if (!mCanceled) {
				animation.start();
			}
		}

		public void stopAnimation() {
			mCanceled = true;
		}
	};

	BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(CruiseControlService.ACTION_SPEED)) {
				mCurrentGpsSpeed = intent.getExtras().getFloat(CruiseControlService.EXTRA_SPEED);
				mCurrentGpsSpeedAngle = metersPerSecondToAngle(mCurrentGpsSpeed);
				mPinAnimation.setFloatValues((float) mCurrentGpsSpeedAngle); 
				mPinAnimation.start();
				dText3.setText(String.format("%.1f", mCurrentGpsSpeed * (!mImperial ? 3.6f : 2.23693629f)));
			} else if (action.equals(CruiseControlService.ACTION_OVERSPEED)) {
				mAlarmAnimatorOverSpeed.start();
				mAlarmAnimatorUnderSpeed.end();
				dText1.setText("OVERSPEED");
			} else if (action.equals(CruiseControlService.ACTION_UNDERSPEED)) {
				mAnimatorListener.stopAnimation();
				mAlarmAnimatorUnderSpeed.start();
				dText1.setText("UNDERSPEED");
			} else if (action.equals(CruiseControlService.ACTION_INRANGE)) {
				mAnimatorListener.stopAnimation();
				mAlarmAnimatorUnderSpeed.end();
				dText1.setText("IN RANGE");
			} else if (action.equals(CruiseControlService.ACTION_SHUTDOWN)) {
				Log.d("cc", this.getClass().getName() + ".onReceive: ACTION_SHUTDOWN");
				MetersActivity.this.finish();
			} else if (action.equals(CruiseControlService.ACTION_INACTIVE)) {
				Log.d("cc", this.getClass().getName() + ".onReceive: ACTION_INACTIVE");
				mAnimatorListener.stopAnimation();
				mAlarmAnimatorUnderSpeed.end();
			}
		}
	};

	private CruiseControlService mCruiseControlService;

	CruiseControlServiceConnection mConnection = new CruiseControlServiceConnection();
	
	private class CruiseControlServiceConnection implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			mCruiseControlService = ((CruiseControlService.LocalBinder) arg1).getService();
			if(true /*mCruiseControlService.isGpsProviderEnabled()*/){
				mCruiseControlService.unpause();
				this.initialize();
			} else {
				mCruiseControlService.pause();
				MetersActivity.this.showGpsDialog();
			}
		}
		
		public void initialize(){
			mCruiseControlService.setIsBoundToActivity(true);
			float angle = 90.0f;
			if(mCruiseControlService.getCruiseSpeed() < 0){
				mCruiseControlService.setCruiseSpeed(0/*currentUnitToMetersPerSecond(maxSpeed/2)*/);
			} else {
				angle = (float) metersPerSecondToAngle(mCruiseControlService.getCruiseSpeed());
			}
			
			mCruiseSpeedIndicator.setRotation(angle);
			mCruiseSpeedLowerIndicator.setRotation(angle-(float)metersPerSecondToAngle(mCruiseControlService.cruiserTolerance()));
			mCruiseSpeedIndicatorAngle = (angle/180.0f) * Math.PI;
			mCruiseControlService.updateState();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			if (mCruiseControlService != null) {

			}
		}

	};
	
	private void showGpsDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Go to location settings to enable location services?")
				.setTitle("GPS needed")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface d, int id) {
						d.dismiss();
						MetersActivity.this.startActivity(new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS));
						
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface d, int id) {
								d.cancel();
								MetersActivity.this.finish();
							}
						}).setCancelable(false);
		AlertDialog dialog = builder.create();
		
		dialog.show();
	}
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if(requestCode == REQUESTCODE_LOCATION_SETTINGS){
//			if(mCruiseControlService.isGpsProviderEnabled()){
//				mCruiseControlService.requestLocationUpdates();
//				mConnection.initialize();
//			} else {
//				AlertDialog.Builder builder = new AlertDialog.Builder(this);
//				builder.setMessage("You did not enable location services. Go to location settings to enable location services?")
//						.setTitle("GPS needed")
//						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//
//							public void onClick(DialogInterface d, int id) {
//								MetersActivity.this.startActivityForResult(new Intent(
//										Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUESTCODE_LOCATION_SETTINGS);
//								d.dismiss();
//							}
//						})
//						.setNegativeButton("Cancel",
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface d, int id) {
//										d.cancel();
//										MetersActivity.this.finish();
//									}
//								});
//				AlertDialog dialog = builder.create();
//				dialog.show();
//			}
//		}
//	}

	@SuppressLint("ValidFragment")
    public class UnitDialogFragment extends DialogFragment {

        public UnitDialogFragment() {
        }

        @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setSingleChoiceItems(R.array.unit_values_array, mImperial?1:0, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int which) {
	                	   
	                	   if(which == 1){
	                		   mImperial = true;
	                	   } else {
	                		   mImperial = false;
	                	   }

	                   }}).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SharedPreferences.Editor settings = getSharedPreferences(CruiseControlService.SETTINGS_FILE, Context.MODE_PRIVATE).edit();
							settings.putBoolean(CruiseControlService.SETTINGS_UNIT_IMPERIAL, mImperial);
		                	settings.commit();
		                	mCruiseControlService.updateState();
						}
					});

	        return builder.create();
	    }
	}
}
