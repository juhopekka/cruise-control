package com.fistforge.cruisecontrol;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class TutorialActivity extends Activity {

	private int slideNumber;
	private ImageView mSlide;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_tutorial);
		mSlide = (ImageView)this.findViewById(R.id.tutorial_slide_imageview);
		mSlide.setOnClickListener(clickListener);
		slideNumber = 0;
		getActionBar().hide();
		nextSlide();
	}

	private void nextSlide(){
		slideNumber++;
		switch (slideNumber) {
		case 1:
			mSlide.setImageResource(R.drawable.tutorial_1);
			break;
		case 2:
			mSlide.setImageResource(R.drawable.tutorial_2);
			break;
		case 3:
			mSlide.setImageResource(R.drawable.tutorial_3);
			break;
		case 4:
			this.finish();
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		default:
			break;
		}
	}
	
	private View.OnClickListener clickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			nextSlide();
		}
	};
}
