package com.fistforge.cruisecontrol;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import com.fistforge.cruisecontrol.R;

public class CruiseControlService extends Service {

	private NotificationManager mNotificationManager;
	private LocationManager 	mLocationManager;
	private Vibrator 			mVibrator;
	private SpeedListener 		mSpeedListener;
	private Intent 				mSpeedIntent;
	
	// Intents //
	public static final String ACTION_SPEED 		= "com.fistforge.cruisecontrol.ACTION_SPEED";
	public static final String ACTION_SHUTDOWN 		= "com.fistforge.cruisecontrol.ACTION_SHUTDOWN";
	public static final String ACTION_OVERSPEED		= "com.fistforge.cruisecontrol.ACTION_OVERSPEED";
	public static final String ACTION_UNDERSPEED 	= "com.fistforge.cruisecontrol.ACTION_UNDERSPEED";
	public static final String ACTION_INRANGE 		= "com.fistforge.cruisecontrol.ACTION_INRANGE";
	public static final String ACTION_INACTIVE 		= "com.fistforge.cruisecontrol.ACTION_INACTIVE";
	public static final String EXTRA_SPEED 			= "com.fistforge.cruisecontrol.EXTRA_SPEED";
	public static final String ACTION_SET_SPEED	 	= "com.fistforge.cruisecontrol";
	public static final String EXTRA_PRESET_SPEED 	= "com.fistforge.cruisecontrol.EXTRA_PRESET_SPEED";

	// Preferences //
	public static final String SETTINGS_FILE 			= "settingsfile";
	public static final String SETTINGS_SOUNDS_ON 		= "sounds";
	public static final String SETTINGS_VIBRATION_ON	= "vibration";
	public static final String SETTINGS_LIGHTS_ON 		= "light";
	public static final String SETTINGS_UNIT_IMPERIAL 	= "unit";
	public static final String SETTINGS_FIRST_LAUNCH	= "firstlaunch";
	
	// Speed (m/s) //
	private float mCurrentGpsSpeed 			= 0;
	private float mCruiseSpeed 				= 0;
	private double mCruiseSpeedTolerance 	= 1.2;
	
	// Service state //
	private int mState;
	public static final int STATE_NOT_INITIALIZED 	= -1;
	public static final int STATE_INRANGE 			= 0;
	public static final int STATE_OVERSPEED 		= 1;
	public static final int STATE_UNDERSPEED 		= 2;
	public static final int STATE_INACTIVE 			= 3;
	private boolean mIsPaused						= false;
	private boolean isBoundToActivity 				= false;
	private boolean mIsForeground 					= false;
	private boolean mVibrating 						= false;
	private boolean mStateChanged					= false;
	
	// Sound //
	private CruiseControlSounds mSounds;
	
	// Notification //
    private NotificationCompat.Builder mNotificationBuilder;
	private int mNotificationId 			= 1;
	
	// Settings //
	private boolean mSoundsOn 					= true;
	private boolean mVibrationOn				= false;
	private boolean mImperial					= false;
	
	private long[] mOverSpeedVibrationPattern 	= {200,200};
	private long[] mUnderSpeedVibrationPattern 	= {1000,200,100,200};

    @Override
	public void onCreate() {
		super.onCreate();
		Log.d("cc", this.getClass().getName()+ ".onCreate");
		
		mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		mSounds = new CruiseControlSounds(this);

		mSpeedIntent = new Intent(ACTION_SPEED);
		
		SharedPreferences settings = getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE);
		mSoundsOn = settings.getBoolean(CruiseControlService.SETTINGS_SOUNDS_ON, mSoundsOn);
		mVibrationOn = settings.getBoolean(CruiseControlService.SETTINGS_VIBRATION_ON, mVibrationOn);
		mImperial = settings.getBoolean(CruiseControlService.SETTINGS_UNIT_IMPERIAL, mImperial);

        Intent intent = new Intent(this, MetersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cc_white_icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

		IntentFilter filter = new IntentFilter("poiu");
		filter.addAction(ACTION_SET_SPEED);
		filter.addAction(ACTION_SHUTDOWN);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(mBroadcastReceiver, filter);
		
		if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			requestLocationUpdates();
			updateState();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("cc", this.getClass().getName() + ".onStartCommand");
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	@Override 
	public void onDestroy() {
		Log.d("cc", this.getClass().getName() + ".onDestroy");
		if(mSpeedListener != null) mLocationManager.removeUpdates(mSpeedListener);
		if(mBroadcastReceiver != null) unregisterReceiver(mBroadcastReceiver);
		if(mVibrating) mVibrator.cancel();
		mSounds.stop();
		
		super.onDestroy();
	}
	
	public void requestLocationUpdates(){
		if(mSpeedListener == null){
			mSpeedListener = new SpeedListener();
		}
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mSpeedListener);
	}

	public void updateState(){
		if(mIsPaused && mState != STATE_INACTIVE){
			mStateChanged = true;
			mState = STATE_INACTIVE;
			sendBroadcast(new Intent(ACTION_INACTIVE));
			Log.d("cc", "state -> inactive");
		} else if(!mIsPaused) {
			if (((mCurrentGpsSpeed >= mCruiseSpeed-(mCruiseSpeedTolerance*1/2f) && 
				mCurrentGpsSpeed <= mCruiseSpeed+(mCruiseSpeedTolerance*1/2f)) ||
				mCurrentGpsSpeed == 0)
				&& mState != STATE_INRANGE) {
				mStateChanged = true;;
				mState = STATE_INRANGE;
				sendBroadcast(new Intent(ACTION_INRANGE));
				Log.d("cc", "state -> in range");
			} else if (mCurrentGpsSpeed > mCruiseSpeed+(mCruiseSpeedTolerance*1/2f) && mState != STATE_OVERSPEED) {
			
				mStateChanged = true;
				mState = STATE_OVERSPEED;
				sendBroadcast(new Intent(ACTION_OVERSPEED));
				Log.d("cc", "state -> overspeed");
			} else if (mCurrentGpsSpeed < mCruiseSpeed-(mCruiseSpeedTolerance*1/2f) && (mCurrentGpsSpeed != 0) && mState != STATE_UNDERSPEED){
				mStateChanged = true;
				mState = STATE_UNDERSPEED;
				sendBroadcast(new Intent(ACTION_UNDERSPEED));
				Log.d("cc", "state -> underspeed");
			}
		}
		
		SharedPreferences settings = getSharedPreferences(SETTINGS_FILE, MODE_PRIVATE);
			
		boolean oldSoundsOn = mSoundsOn;
		boolean oldVibrationOn = mVibrationOn;
		boolean oldImperial = mImperial;
		mSoundsOn = settings.getBoolean(CruiseControlService.SETTINGS_SOUNDS_ON, mSoundsOn);
		mVibrationOn = settings.getBoolean(CruiseControlService.SETTINGS_VIBRATION_ON, mVibrationOn);
		mImperial = settings.getBoolean(CruiseControlService.SETTINGS_UNIT_IMPERIAL, mImperial);
		boolean settingsChanged = mSoundsOn != oldSoundsOn ||
								  mVibrationOn != oldVibrationOn || 
								  mImperial != oldImperial;
			
		mStateChanged = mStateChanged || settingsChanged;
						
			
		if(mStateChanged){
			handleSound();
			handleVibration();
		}
			
		handleNotification();
			
		mStateChanged = false;
	}
	
	private void handleNotification(){
		if(isBoundToActivity) {
			if(mIsForeground){
				stopForeground(true);
				mIsForeground = false;
				Log.d("cc", "notification -> stop");
			}
		} else {
			if(!mIsForeground){
				startForeground(mNotificationId, buildNotification("0.0" + (!mImperial ?" km/h":" mph")));
				mIsForeground = true;
				Log.d("cc", "notification -> start");
			} else {
				mNotificationManager.notify(mNotificationId, buildNotification(String.format("%.1f", mCurrentGpsSpeed * (!mImperial ? 3.6f : 2.23693629f)) + (!mImperial ?" km/h":" mph") ));
				Log.d("cc", "notification -> update");
			}
		}
	}
	
	private void handleSound(){
		try {
			if(!mSoundsOn || mState == STATE_INACTIVE){
				mSounds.stop();
			} else {
				if (mState == STATE_OVERSPEED) {
					mSounds.playOverSpeed();
					Log.d("cc", "sound -> on");
				} else if (mState == STATE_UNDERSPEED) {
					mSounds.playUnderSpeed();
					Log.d("cc", "sound -> on");
				} else if (mState == STATE_INRANGE) {
					mSounds.stop();
					Log.d("cc", "sound -> off");
				}
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}

	private void handleVibration(){
		if(mState == STATE_OVERSPEED  && mVibrationOn){
			mVibrator.vibrate(mOverSpeedVibrationPattern, 0);
			mVibrating = true;
		} else if(mState == STATE_UNDERSPEED  && mVibrationOn){
			mVibrator.vibrate(mUnderSpeedVibrationPattern, 0);
			mVibrating = true;
		}else if((mState == STATE_INRANGE && mVibrating) || mState == STATE_INACTIVE || !mVibrationOn){
			mVibrator.cancel();
			mVibrating = false;
		}
	}

	private Notification buildNotification(String text){

        mNotificationBuilder.setContentText(text);

		/*Notification.Builder builder = new Notification.Builder(this)
			.setSmallIcon(R.drawable.cc_white_icon)
			.setContentTitle("My notification")
			.setContentText("Hello World!");*/

		// Set return-to-application action
		
		/*RemoteViews remoteView = new RemoteViews(this.getPackageName(), R.layout.notification);
		remoteView.setTextViewText(R.id.notification_description_textview, text);
		remoteView.setOnClickPendingIntent(R.id.notification_close_button, 
				PendingIntent.getBroadcast(this, 0, new Intent(ACTION_SHUTDOWN), 
				PendingIntent.FLAG_UPDATE_CURRENT));
		builder.setContent(remoteView);*/
		return mNotificationBuilder.build();
	}
	
	BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(action.equals(ACTION_SHUTDOWN)){
				Log.d("cc", this.getClass().getName() + ".onReceive: ACTION_SHUTDOWN");
				CruiseControlService.this.stopForeground(true);
				CruiseControlService.this.stopSelf();
			} else if(action.equals(Intent.ACTION_SCREEN_OFF)){
				Log.d("cc", this.getClass().getName() + ".onReceive: ACTION_SCREEN_OFF");
				mVibrating = false;
				mStateChanged = true;
				CruiseControlService.this.updateState();
			} else if(action.equals("poiu")){
				//Log.d("cc", "current speed: " + mCurrentGpsSpeed);
				float speed = intent.getExtras().getFloat(CruiseControlService.EXTRA_SPEED);
				mCurrentGpsSpeed = speed;
				mSpeedIntent.putExtra(EXTRA_SPEED, mCurrentGpsSpeed);
				sendBroadcast(mSpeedIntent);
				updateState();
			} 
		}
	};
	
	private class SpeedListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {

			if (location.hasSpeed()) {
				mCurrentGpsSpeed = location.getSpeed();
				mSpeedIntent.putExtra(EXTRA_SPEED, mCurrentGpsSpeed);
				sendBroadcast(mSpeedIntent);
				updateState();
			}
		}

		@Override
		public void onProviderDisabled(String arg0) {}

		@Override
		public void onProviderEnabled(String arg0) {}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}
	}

	public double cruiserTolerance(){
		return mCruiseSpeedTolerance;
	}
	
	public boolean isGpsProviderEnabled(){
		return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
	
	public void setIsBoundToActivity(boolean b) {
		isBoundToActivity = b;
	}

	public void setCruiseSpeed(float speed) {
		mCruiseSpeed = speed;
		Log.d("cc", "cruise speed set to: " + mCruiseSpeed + " m/s");
	};
	
	public float getCruiseSpeed(){
		return mCruiseSpeed;
	}
	
	public void pause(){
		mIsPaused = true;
		this.updateState();
	}
	
	public void unpause(){
		mIsPaused = false;
		if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			requestLocationUpdates();
			updateState();
		}
	}
	
	private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        CruiseControlService getService() {
            return CruiseControlService.this;
        }

    }
}
